//MONTENEGRO BLAS ANTON AUXILIAR DE 2da CATEGORIA INFORMATICA.
//UNIVERSIDAD NACIONAL DE ROSARIO - FACULTAD DE CIENCIAS EXACTAS INGENIERIA y AGRIMENSURA.
//ESCUELA DE FORMACION BASICA - DEPARTAMENTO DE MATEMATICA - INFORMATICA 1 - COMISION 190.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
     char codr[4],nombr[30];
     int cat ;
     }regiones;

void carga_r(char caracter[29][4]){
    FILE*a1;
    char c[30];
    int i, m;
    			printf("Intenetado abrir REGIONES.dat . . .\n");
	a1= fopen("REGIONES.DAT","r");
        		printf("Archivo encontrado y abierto . . .\n");
	i = 0;
	    		printf("Cargando Informacion en el espacio de Memoria . . .\n");
	   while(!(feof(a1)))
    {
		fscanf(a1,"%s %s %d", caracter[i], c, &m);
        i = i+1;
    }
        		printf("Carga Completa . . .\n");
    fclose(a1);
        		printf("Cerrando el archivo RAGIONES.dat . . .\n\n");
}

void r(int result){
    regiones r[29];
    FILE *a1 ;
    int categoria,i,j,p;
    char codigo[4],nombre[30];

    a1 = fopen("REGIONES.DAT","r");
    i = 0;
    p = 0;
      fscanf(a1,"%s %s %d", codigo, nombre, &categoria);
    while(!(feof(a1)))
    {

        if (categoria == result)
        {

        		i = i+1;
        		printf("\n%s\t%s\t\n",codigo,nombre);
        }
         		fscanf(a1,"%s %s %d", codigo, nombre, &categoria);
        		p=p+1;
    }
    if(i == 0)
    {
        printf("La categoria %d no existe\n", result);
    }
     else
     {
        printf("\nEl total de regiones es %d\n", i);
     }
close(a1);}

float acumulador(char cod1[4]){

int i, mes;
float acum,mm;
char cod[4];

FILE *a;
a=fopen("LLUVIA_REG.DAT","r");
i=0;
acum=0;
while(!(feof(a))) {
    fscanf(a,"%s %f %d",cod,&mm,&mes);
    if(!(strcmp(cod1,cod))){
        acum=acum+mm;
        }
}
fclose(a);
return(acum);
}

int mlluvia(float T[29],float num){

int i,cont;

cont=0;
for(i=0;i<29;i++){
	     if(T[i]> num) {
	    cont=cont+1;
        }
	}
        return(cont);
}

int men150(float T[29]){

int cant, i;

cant=0;
for(i=1;i<29;i++){
    if(T[i]<150){
        cant=cant+1;
        }
}
        return(cant);
}

void ordenar(char cod[29][4],float mm[29]) {

char mayorcod[4];
int i,n=29,j,lugar,mes;
float mayormm;

    printf("Intenetado crear el archivo ANUAL.dat . . .\n");
FILE*a;
a=fopen("ANUAL.dat","w");
    printf("Archivo creado satisfactoriamente . . .\nOrdenando la tabla de datos . . .\n");
for(i=0;i<n;i++){

       strcpy(mayorcod,cod[i]);
       mayormm=mm[i];
       lugar=i;
       for(j=i+1;j<n;j++) {
		 if(mm[j]>mayormm){
               mayormm=mm[j];
         strcpy(mayorcod,cod[j]);
         lugar=j;
         }

         }
         mm[lugar]=mm[i];
mm[i]=mayormm;
strcpy(cod[lugar],cod[i]);
strcpy(cod[i],mayorcod);

         }
             printf("Ordenamiento completo . . .\nEscribiendo el archivo ANUAL.dat . . .\n");
         for(i=0;i<n;i++){
            fprintf(a,"%s %f\n",cod[i],mm[i]);
         }
    printf("Archivo ANUAL.dat escrito completamente . . .\nCerrando archivo ANUAL.dat . . .\n");
fclose(a);
    printf("Archivo ANUAL.dat cerrado satisfactoriamente. . . .\n\n");
}

void cargaranual(char co[29][4],float t[29]){

     int i,cat;
     char cod[29][4],nom[31];
    printf("Calculando total de mm de lluvia por region . . .\n");
    for(i=0;i<29;i++){
       t[i]=acumulador(co[i]);
    }
    printf("Calculo completo de total de mm de lluvia por region completo . . .\n\n");
}

void REGISTRO(char cod[29][4] ,float mm[29]){

int i ;

 carga_r(cod);
 cargaranual(cod,mm);
 ordenar(cod,mm);

}

void MENU(char *resp){

system("pause");
system("cls");
system("color F0");
printf("***************************************************\n");
printf("***************** MENU DE OPCIONES ****************\n");
printf("***************************************************");
printf("\n***** M-Cantidad de regiones con mucha lluvia *****");
printf("\n***** S-Cantidad de regiones secas            *****");
printf("\n***** R-Regiones                              *****");
printf("\n***** F-Fin                                   *****\n");
printf("***************************************************\n\n***************************************************");
printf("\nSeleccione una opcion: ");
scanf(" %c",resp);
}



int main () {

char opcion='A', cod[29][4];
int cat,mucha,cant,H,reg, seca;
float num, mm[29], t[29];

REGISTRO(cod,mm);
while(opcion!='F'){
MENU(&opcion);
switch (opcion) {
        case 'M':
            printf ("\nIngrese un numero real: ");
            scanf(" %f", &num);
            mucha=mlluvia(mm,num);
            printf("\nLa cantidad de regiones cuya lluvia anual fue superior a: %.2f es %d\n",num,mucha);
        break;
        case 'S':
            seca=men150(mm);
            printf("\nLa cantidad de regiones cuya lluvia anual fue menor a 150mm es: %d\n",seca);
        break;
        case 'R':
            printf("\nIngrese la categoria de la region: ");
            scanf(" %d",&cat);
            r(cat);
        break;
        case 'F':
            printf("\nSaludos, comision 190.");
        break;
        default: printf("\t\nLA CATEGORIA # NO EXISTE\n");
    }


}

}
