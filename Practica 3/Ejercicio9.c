/* Ejercicio 9
Datos: Numero positivos (cantidad incierta de numeros) Reales
Resultados: Promedio de los positivos ingresados
Metodologia: Al ingresar N<0 finalizo la carga de datos <- Fin de datos
	Ingresar N
	Repetir mientras (N>=0)
		suma<-suma+N
		cont<-cont+1
		ingresar N
	fin mientras
 
 Algoritmo Promedio
 
 Variables
 Real: acum, N
 entero: cont
 
 Inicio
 acum<-0
 cont<-0
 Escribir("Ingrese los numeros a promediar, o un negativo para finalizar la carga")
 Leer(N)
 Repetir mientras(N>0)
 	acum<-acum+N
 	cont<-cont+1
 	Escribir("Ingrese los numeros a promediar, o un negativo para finalizar la carga")
 	Leer(N)
 fin mientras
 Si (cont==0)
 	Escribir("No se ingresaron numeros")
 sino
 	Escribir("El promedio de los numeros es:" acum/cont)
 fin si
 Fin*/
 
 #include<stdio.h>
 
 main(){
 
 float acum, N;
 int cont;
 
 acum=0;
 cont=0;
 printf("Ingrese los numeros a promediar, o un negativo para finalizar la carga:\n");
 scanf("%f", &N);
 while(N>=0){
 	acum=acum+N;
 	cont=cont+1;
 	printf("Ingrese los numeros a promediar, o un negativo para finalizar la carga:\n");
 	scanf("%f", &N);
 }
 if (cont==0){
 	printf("No se ingresaron numeros");
}else{
 	printf("El promedio de los numeros es: %f", acum/cont);
}
 }
