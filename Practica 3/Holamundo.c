/* Ejemplo hola mundo */
#include<stdio.h>

main(){ 										//Nombre de mi algoritmo y la { es el inicio del algoritmo en pseudocodigo, 
												//Observar que { es analogo a Inicio en pseudocodigo.-
	
	int N;										//Delcaro una variable de tipo entero (int seria entero.-)
	
	printf("Hola mundo"); 						//printf es el Escribir del pseudocodigo, printf significa, imprimir con formato. 
												//No olvidar el ;, todas las instruccion terminan con ;.-
	
	printf("\nIngrese un numero: ");			//para dejar un renglon entre printf y printf utilizo el \n.-
	
	scanf("%d", &N);							//scanf es el leer del pseudocodigo, es escanear con formato, 
												//el %d significa que lo que voy a leer es un entero, el &N es que voy a guardar ese valor
												//en el lugar donde esta la variable N, respetar los espacios, formato y variables.-
												
	if(N>=0){									//el if es el "si" del pseudocodigo, vuelvo a utilizar los { para marcar bloques
	
	printf("El numero %d es positivo",N);		//el %d deja un hueco que se llenara con la primer variable de la lista que coloco, en este caso, la N.-
	
	}											//El } marca el fin si, fin del bloque del if que abri lineas anteriores, no lleva ;.-
	
	else{										//Else significa sino del pseudocodigo
	
	printf("El numero %d es negativo",N);		//Podria introducir mas huecos y mas variables, 
												//se iran cumpliendo en el mismo orden que son escritas en la oracion							 	
}
												//En este caso, el } marca el fin del bloque del sino, o en este caso, del else												
}												//El simbolo } marca el fin del algoritmo, es analogo a "fin" del pseudocodigo.-
