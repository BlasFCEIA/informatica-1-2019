/*Ejercicio 8 de Practica 3

Datos: Numeros Naturales del 1 al 200
Resultados: 1: Suma de los pares
			2: Suma de los multiplos de 5
Metodologia: Si (mod(i,2) == 0) entonces Acumulamos
			 Si (mod(i,5) == 0) entonces Acumulamos
			 
Algoritmo Prob3_Ej8

Variables
	entero: i, suma2, suma5
Inicio
suma2 <- 0
repetir para i<-0, 200
		si (mod(i,2)==0) entonces
			suma2<-suma2+i
		fin si
fin para
Escribir("La suma de los pares del 1 al 200 entoces es: " suma2)
i<-1
suma5<-0
repetir mientras (i<201)
		si (mod(i,5)==0) entonces
			suma5<-suma5+i
		fin si
		i<-i+1
fin mientras
Escribir("La suma de los multiplos de 5 del 1 al 200 entoces es: " suma5)
Fin*/
#include <stdio.h>
main(){
	int i, suma;
suma = 0;
for (i=0;i<=200;i++){
		if (i%2==0)
			suma=suma+i;
}
printf("La suma de los pares del 1 al 200 entoces es: %d\n", suma);
i=1;
suma=0;
while (i<201){
		if (i%5==0)
			suma=suma+i;
		i=i+1;
}
printf("La suma de los multiplos de 5 del 1 al 200 entoces es: %d", suma);
}
